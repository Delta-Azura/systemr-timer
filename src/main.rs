use std::env;
use std::fs::File;
use std::fs;
use std::path::Path;
use std::{thread, time};
use std::process::Command;
use std::process::exit;
use notify_rust::Notification;

fn main() {
    let args: Vec<String> = env::args().collect();
	let fonction = &args[1];

	if fonction == "fix" {
		fix();
	}

    if fonction == "about" {
        apropos();
    }

    if fonction == "timer" {
        timer();
    }

    if fonction == "limit" {
        limited();
    }

    if fonction == "help" {
        help();
    }

    if fonction == "launch" {
        launch_timer();
    }

    if fonction == "conf" {
        let args: Vec<String> = env::args().collect();
        let time_or_path = &args[2];
        if time_or_path == "time" {
            conf_time();
        } else {
            conf_path();
        }
    }
}

fn timer() {
    let args: Vec<String> = env::args().collect();
	let name = &args[2];
    let chemin = &args[3];
    let temps = &args[4];
    let dir = format!("/etc/systemr/time/script/{}", name);
    let create = Path::new("/etc/systemr/time/script/");
    assert!(env::set_current_dir(&create).is_ok());
    fs::create_dir(name);
    let scripts = Path::new(&dir);
    assert!(env::set_current_dir(&scripts).is_ok());
    File::create("path");
    fs::write("path", chemin);
    File::create("time");
    fs::write("time", temps);
    readtime();
}

fn limited() {
    println!("Le compte à rebours est lancé !");
    readtime();
    println!("Le temps est écoulé !");
    Notification::new()
        .summary("Temps écoulé !")
        .body("Le script d'arrêt se lancera dans 1 min")
        .icon("firefox")
        .show();
    let millis = time::Duration::from_millis(60000);
    let now = time::Instant::now();
    thread::sleep(millis);
    std::process::Command::new("poweroff").output().expect("Erreur");
}

fn conf_time() {
    let args: Vec<String> = env::args().collect();
	let time = &args[3];
    fs::write("/etc/systemr/time/script/limit/time", time);
}

fn conf_path() {
    let args: Vec<String> = env::args().collect();
	let path = &args[3];
    fs::write("/etc/systemr/time/script/limit/path", path);
}

fn launch_timer() {
    let args: Vec<String> = env::args().collect();
	let name = &args[2];
    let dir = format!("/etc/systemr/time/{}", name);
    println!("{}", dir);
    let launch = Path::new(&dir);
    assert!(env::set_current_dir(&launch).is_ok());
    let time = fs::read_to_string("time").expect("Quelque chose s'est mal passé lors de la lecture du fichier");
    let time = time.trim().parse::<u64>().unwrap();
    let millis = time::Duration::from_millis(time);
    let now = time::Instant::now();
    thread::sleep(millis);
    let run = fs::read_to_string("path").expect("Failed");
    std::process::Command::new(run).output().expect("Failed");
}

fn fix() {
    std::process::Command::new("pacman").arg("-S").arg("base-devel").arg("--needed").output();
    fs::remove_dir_all("/etc/systemr");
    fs::remove_dir_all("/var/systemr/scripts/limit");
    fs::create_dir_all("/etc/systemr/time/script/limit");
    File::create("/etc/systemr/time/script/limit/time");
    fs::create_dir_all("/var/systemr/scripts/");
    File::create("/var/systemr/scripts/limit");
    let time = "6000000";
    fs::write("/etc/systemr/time/script/limit/time", time);
    println!("Done");
}

fn readtime() {
    let file = "/etc/systemr/time/script/limit/time";
    let contenu = fs::read_to_string(file).expect("Quelque chose s'est mal passé lors de la lecture du fichier");
    let args: Vec<String> = env::args().collect();
	let fonction = &args[1];
    println!("Il vous reste exactement {} millisecondes !", contenu);
    let time = contenu.trim().parse::<u64>().unwrap();
    let millis = time::Duration::from_millis(time);
    let now = time::Instant::now();
    thread::sleep(millis);
}

fn help() {
    println!("Veuillez lancer fix à la première éxécution !");
    println!("Limit pour lancer le compte à rebour avant d'éteindre le pc !");
    println!("sudo timer pour configurer l'éxécution d'un script au bout du temps définit dans les fichiers de conf");
    println!("Launch pour lancer le timer configurer ( avec sudo si nécessaire ! )")
}

fn apropos() {
    println!("Systemr-timer est un gestionnaire de timer !")
}